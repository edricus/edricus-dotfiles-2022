;=============================================================;
;																														  ;
;																														  ;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗	  ;
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗	  ;
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝	  ;
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗	  ;
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║	  ;
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝	  ;
;																														  ;
;																														  ;
;=============================================================;

[colors]
background = #282A2E
background-alt = #373B41
foreground = #C5C8C6
primary = #F0C674
secondary = #8ABEB7
alert = #A54242
disabled = #707880

[global/wm]
include-file = $HOME/.config/polybar/macchiato.ini

[bar/example]
width = 100%
height = 18pt
radius = 0
background = ${colors.base}
foreground = ${colors.text}
line-size = 0pt
vertical-offset = 10
border-size = 0pt
border-color = #00000000
padding-left = 0
padding-right = 4
module-margin = 2
separator = 
separator-foreground = ${colors.disabled}
enable-ipc = true
cursor-click = pointer
cursor-scroll = ns-resize

font-0 = SF-Pro Display:style=Medium:size=9;2
font-1 = Font Awesome 5 Free:style=Solid:size=11;2
font-2 = Font Awesome 5 Free:style=Regular:size=11;2
font-3 = Font Awesome 5 Brands:style=Regular:size=11;2
font-4 = Unifont:style=Regular;2

modules-left = i3 title
modules-center = date
modules-right = pulseaudio backlight wlan battery

[module/i3]
type = internal/i3
pin-workspaces = true
show-urgent = true
strip-wsnumbers = true
index-sort = true
enable-click = true
enable-scroll = false
wrapping-scroll = false
reverse-scroll = false
fuzzy-match = true

format = <label-state> <label-mode>
ws-icon-0 = 1;♚
ws-icon-1 = 2;♛
ws-icon-2 = 3;♜
ws-icon-3 = 4;♝
ws-icon-4 = 5;♞
ws-icon-default = ♟

label-mode = %mode%
label-mode-padding = 3
label-mode-background = ${colors.sapphire}
label-mode-foreground = ${colors.base}

label-focused = %index%
label-focused-foreground = ${colors.text}
label-focused-background = ${colors.crust}
label-focused-underline = #fba922
label-focused-padding = 3

label-unfocused = %index%
label-unfocused-padding = 3

label-visible = %index%
label-visible-underline = #555555
label-visible-padding = 3

label-urgent = %index%
label-urgent-foreground = ${colors.text}
label-urgent-background = ${colors.red}
label-urgent-padding = 3

label-separator = 
label-separator-padding = 0
label-separator-foreground = #ffb52a

[module/title]
type = internal/xwindow
format = <label>
format-background = ${colors.base}
format-foreground = ${colors.text}
format-padding = 2

label = %title%
label-maxlen = 150

label-empty = 
label-empty-foreground = #707880

[module/date]
type = internal/date
interval = 1
date = %d %b %H:%M
date-alt = %Y-%m-%d %H:%M:%S
label = %date%
label-foreground = ${colors.text}

[module/pulseaudio]
type = internal/pulseaudio

format-volume-prefix = "  "
format-volume-prefix-foreground = ${colors.blue}
format-volume = <bar-volume>
format-volume-prefix-padding = 2
bar-volume-width = 10
bar-volume-empty = ─
bar-volume-empty-foreground = ${colors.base}
bar-volume-fill = ─
bar-volume-fill-font = 4
bar-volume-indicator = ─
bar-volume-indicator-font = 4
label-volume = %percentage%%

label-muted = " "
label-muted-padding = 2
label-muted-foreground = ${colors.disabled}

[module/backlight]
type = internal/backlight
card = amdgpu_bl0
use-actual-brightness = true
enable-scroll = false
format = <bar>
format-prefix = " "
format-prefix-foreground = ${colors.blue}
format-prefix-padding = 2
bar-width = 10
bar-indicator = ─
bar-fill = ─
bar-empty-foreground = ${colors.base}
bar-empty = ─

[module/battery]
type = internal/battery
full-at = 99
low-at = 5
battery = BAT0
adapter = ADP1
poll-interval = 5
time-format = %H:%M
bar-capacity-width = 10

format-full-prefix = " "
label-full = %percentage%%
format-full-foreground = ${colors.text}
format-full-prefix-foreground = ${colors.green}
format-full-prefix-padding = 2

format-charging = <animation-charging>  <label-charging>
label-charging = %percentage%%
animation-charging-foreground = ${colors.blue}
format-charging-padding = 2
animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-3 = 
animation-charging-4 = 
animation-charging-framerate = 1000

format-discharging = <ramp-capacity>  <label-discharging>
label-discharging = %percentage%%
ramp-capacity-foreground = ${colors.blue}
format-discharging-padding = 2
ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-3 = 
ramp-capacity-4 = 

label-low = %percentage%%
low-animation-1 = " "
low-animation-2 = " "
low-animation-foreground = ${colors.red}
format-low-foreground = ${colors.text}
animation-low-framerate = 200

[network-base]
type = internal/network
interval = 5
format-connected = <label-connected>
format-disconnected = <label-disconnected>
label-disconnected = Disconnected
format-disconnected-prefix = " "

[module/wlan]
inherit = network-base
interface-type = wireless
label-connected = %essid%
label-connected-foreground = ${colors.text}
format-connected-prefix = " "
format-connected-foreground = ${colors.blue}
format-connected-prefix-padding = 2
format-connected-padding = 2

[settings]
screenchange-reload = true
pseudo-transparency = true

; vim:ft=dosini
